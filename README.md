# README #

This project is designed as a test for Java web developers. Feel free to download it, import it into your preferred developmente system (it was originally developed with Eclipse) and show your skills.

Here some hint to build the development suite without hassle.

Do a 

`git clone https://bitbucket.org/eurostarsl/test-webdev-1.git`

on a directory of your choice.

Run Eclipse, and do a

`File, Import, Maven, Exixting Maven projects`

Select in 'Root Directory' the folder in which you got the git clone. The form has to be like this:

![Eclipse maven project import.png](https://bitbucket.org/repo/dAbRza/images/2251347336-Eclipse%20maven%20project%20impot.png)

Click `Finish`.

Wait for Eclipse to end the import, right-click on the 'crewselector' project, and do a 

`Maven, update project`

Set the flag `Force update of Snpashot/Releases`

After all the downloads the project will be ready. 

Now for the run/debug.

The servlet container used for development is Tomcat 7.0.54, which you can download [here](http://tomcat.apache.org/download-70.cgi).

Download the 'core-zip' and unzip it in a folder of your choose.

Open the 'Servers' view in Eclipse, right click on the background of the view, 'New, Server'.

Choose the Tomcat 7 server, like in the following image:

![Eclipse Tomcat server conf 1 .png](https://bitbucket.org/repo/dAbRza/images/3099988330-Eclipse%20Tomcat%20server%20conf%201%20.png)

Click on `Configure runtime environments`, `add`, and configure the options like in the following image:

![Eclipse Tomcat server conf 2 .png](https://bitbucket.org/repo/dAbRza/images/4180398855-Eclipse%20Tomcat%20server%20conf%202%20.png)

Next, and:

![Eclipse Tomcat server conf 3 .png](https://bitbucket.org/repo/dAbRza/images/92754564-Eclipse%20Tomcat%20server%20conf%203%20.png)

Click 'Finish'.

Now you have Tomcat installed. Drag the project 'crewselector' over the newly installed server, and you are ready to run it.

Right-click on the server, and Start.

Now point a browser on http://localhost:8080/crewselector, and you'll see the home page of the project. There you'll find the details of the test.

You're ready!

GOOD LUCK!