package es.eurostar.crewselector.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import es.eurostar.crewselector.dao.CrewSelectorDao;
import es.eurostar.crewselector.model.Role;
import es.eurostar.crewselector.model.User;

/**
 * Handles requests for the application home page.
 */
@Controller
@SessionAttributes("loggedUser")
public class AddUserController {

	private static final Logger logger = LoggerFactory
			.getLogger(AddUserController.class);
	
	@Autowired
	private CrewSelectorDao userDao;
	
	@ModelAttribute("roles")
	public Role[] roles() {
		return userDao.getRoles();
	}

	@RequestMapping(value = "/adduser", method = RequestMethod.GET)
	public ModelAndView  initForm(@ModelAttribute("loggedUser") User loggedUser, ModelMap model) {
		User data = new User();
		if (loggedUser.getFunctions().contains("useradmin")){
			data.setIdrole(1);
			model.addAttribute("user", data);
			ModelAndView mav = new ModelAndView();
			mav.addObject("user", data);
			return new ModelAndView("adduser","user",data);
		}
		return new ModelAndView("forbidden");
	}
	
	@RequestMapping(value = "/adduser", method = RequestMethod.POST)
	public String addUser(@ModelAttribute("user") User user,  ModelMap model) {

		logger.info("Users in db before: "+userDao.getUsersCount());
		userDao.saveUser(user);
		model.addAttribute("login", user.getIdLogin());
		model.addAttribute("passwd", user.getPasswd());
		model.addAttribute("name", user.getName());
		model.addAttribute("surname", user.getSurname());
		model.addAttribute("note", user.getNote());
		model.addAttribute("idrole", user.getIdrole());
		
		logger.info("Users in db after: "+userDao.getUsersCount());
 
		return "redirect:/";
	}

}
