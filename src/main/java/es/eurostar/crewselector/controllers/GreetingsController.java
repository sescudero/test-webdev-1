package es.eurostar.crewselector.controllers;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.eurostar.crewselector.dao.CrewSelectorDao;

/**
 * Handles requests for the application home page.
 */
@Controller
public class GreetingsController {

	private static final Logger logger = LoggerFactory
			.getLogger(GreetingsController.class);
	
	@Autowired
	private CrewSelectorDao userDao;

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/greetings", method = RequestMethod.GET)
	public String greetings(Locale locale, Model model) {
		logger.info("The client locale is {}.", locale);

	
		return "greetings";
	}

}
