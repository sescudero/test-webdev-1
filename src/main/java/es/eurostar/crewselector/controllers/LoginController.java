package es.eurostar.crewselector.controllers;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import es.eurostar.crewselector.dao.CrewSelectorDao;
import es.eurostar.crewselector.model.User;
import es.eurostar.crewselector.services.UserService;

/**
 * Handles requests for the application home page.
 */
@Controller
@SessionAttributes("loggedUser")
public class LoginController {

	private static final Logger logger = LoggerFactory
			.getLogger(LoginController.class);
	
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private CrewSelectorDao userDao;
	
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String initForm(Locale locale, Model model) {
		
		model.addAttribute("loginPasswd", new User());
		
		return "login";
	}

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(@ModelAttribute("loginPasswd") User user, Locale locale, Model model) {
		User loggedUser = userDao.isLoginOK(user);
		if (loggedUser!=null){
			logger.info("Login succesful");
			
			loggedUser.setFunctions(userService.getFunctionsByRoles(loggedUser.getIdrole()));
			
			model.addAttribute("loggedUser", loggedUser);
			return "greetings";
		}
		return "wronglogin";
	}

}
