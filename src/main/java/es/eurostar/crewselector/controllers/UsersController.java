package es.eurostar.crewselector.controllers;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.eurostar.crewselector.dao.CrewSelectorDao;
import es.eurostar.crewselector.model.User;
import es.eurostar.crewselector.services.UserService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class UsersController {

	private static final Logger logger = LoggerFactory
			.getLogger(UsersController.class);
	
	@Autowired
	private CrewSelectorDao userDao;
	
	@Autowired
	private UserService userService;
	
	@ModelAttribute("users")
	public User[] users() {
		
		return userService.getUsers();
	}

	private boolean sortAscending = true; 
	
	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public String users(Locale locale, Model model) {
		logger.info("The client locale is {}.", locale);

		return "users";
	}
	
	@RequestMapping(value = "/users/orderByIdUser", method = RequestMethod.GET)
	public String orderByIdUser(Locale locale, Model model) {
		logger.info("orderByIdUser", locale);

		Map<String,Object> attributesMap = model.asMap();
		
		User[] users = (User[]) attributesMap.get("users");
		
		if (sortAscending){
			Arrays.sort(users, new Comparator<User>() {
				@Override
				public int compare(User o1, User o2) {
					return new Integer(o1.getIduser()).compareTo(new Integer(o2.getIduser()));
				}
			});
			sortAscending = false;
		}
		else{
			Arrays.sort(users, new Comparator<User>() {
				@Override
				public int compare(User o1, User o2) {
					return new Integer(o2.getIduser()).compareTo(new Integer(o1.getIduser()));
				}
			});
			sortAscending = true;
		}
		return "users";
	}
	
	@RequestMapping(value = "/users/orderByLogin", method = RequestMethod.GET)
	public String orderByLogin(Locale locale, Model model) {
		logger.info("orderByLogin", locale);

		Map<String,Object> attributesMap = model.asMap();
		
		User[] users = (User[]) attributesMap.get("users");
		
		if (sortAscending){
			Arrays.sort(users, new Comparator<User>() {
				@Override
				public int compare(User o1, User o2) {
					return o1.getIdLogin().compareTo(o2.getIdLogin());
				}
			});
			sortAscending = false;
		}
		else{
			Arrays.sort(users, new Comparator<User>() {
				@Override
				public int compare(User o1, User o2) {
					return o2.getIdLogin().compareTo(o1.getIdLogin());
				}
			});
			sortAscending = true;
		}
		return "users";
	}
	
	@RequestMapping(value = "/users/orderByPasswd", method = RequestMethod.GET)
	public String orderByPasswd(Locale locale, Model model) {
		logger.info("orderByPasswd", locale);

		Map<String,Object> attributesMap = model.asMap();
		
		User[] users = (User[]) attributesMap.get("users");
		
		if (sortAscending){
			Arrays.sort(users, new Comparator<User>() {
				@Override
				public int compare(User o1, User o2) {
					return o1.getPasswd().compareTo(o2.getPasswd());
				}
			});
			sortAscending = false;
		}
		else{
			Arrays.sort(users, new Comparator<User>() {
				@Override
				public int compare(User o1, User o2) {
					return o2.getPasswd().compareTo(o1.getPasswd());
				}
			});
			sortAscending = true;
		}
		return "users";
	}
	
	@RequestMapping(value = "/users/orderByName", method = RequestMethod.GET)
	public String orderByName(Locale locale, Model model) {
		logger.info("orderByName", locale);

		Map<String,Object> attributesMap = model.asMap();
		
		User[] users = (User[]) attributesMap.get("users");
		
		if (sortAscending){
			Arrays.sort(users, new Comparator<User>() {
				@Override
				public int compare(User o1, User o2) {
					return o1.getName().compareTo(o2.getName());
				}
			});
			sortAscending = false;
		}
		else{
			Arrays.sort(users, new Comparator<User>() {
				@Override
				public int compare(User o1, User o2) {
					return o2.getName().compareTo(o1.getName());
				}
			});
			sortAscending = true;
		}
		return "users";
	}
	
	@RequestMapping(value = "/users/orderBySurname", method = RequestMethod.GET)
	public String orderBySurname(Locale locale, Model model) {
		logger.info("orderBySurname", locale);

		Map<String,Object> attributesMap = model.asMap();
		
		User[] users = (User[]) attributesMap.get("users");
		
		if (sortAscending){
			Arrays.sort(users, new Comparator<User>() {
				@Override
				public int compare(User o1, User o2) {
					return o1.getSurname().compareTo(o2.getSurname());
				}
			});
			sortAscending = false;
		}
		else{
			Arrays.sort(users, new Comparator<User>() {
				@Override
				public int compare(User o1, User o2) {
					return o2.getSurname().compareTo(o1.getSurname());
				}
			});
			sortAscending = true;
		}
		return "users";
	}
	
	@RequestMapping(value = "/users/orderByNote", method = RequestMethod.GET)
	public String orderByNote(Locale locale, Model model) {
		logger.info("orderByNote", locale);

		Map<String,Object> attributesMap = model.asMap();
		
		User[] users = (User[]) attributesMap.get("users");
		
		if (sortAscending){
			Arrays.sort(users, new Comparator<User>() {
				@Override
				public int compare(User o1, User o2) {
					return o1.getNote().compareTo(o2.getNote());
				}
			});
			sortAscending = false;
		}
		else{
			Arrays.sort(users, new Comparator<User>() {
				@Override
				public int compare(User o1, User o2) {
					return o2.getNote().compareTo(o1.getNote());
				}
			});
			sortAscending = true;
		}
		return "users";
	}
	
	@RequestMapping(value = "/users/orderByIdRole", method = RequestMethod.GET)
	public String orderByIdRole(Locale locale, Model model) {
		logger.info("orderByIdRole", locale);

		Map<String,Object> attributesMap = model.asMap();
		
		User[] users = (User[]) attributesMap.get("users");
		
		if (sortAscending){
			Arrays.sort(users, new Comparator<User>() {
				@Override
				public int compare(User o1, User o2) {
					return new Integer(o1.getIdrole()).compareTo(new Integer(o2.getIdrole()));
				}
			});
			sortAscending = false;
		}
		else{
			Arrays.sort(users, new Comparator<User>() {
				@Override
				public int compare(User o1, User o2) {
					return new Integer(o2.getIdrole()).compareTo(new Integer(o1.getIdrole()));
				}
			});
			sortAscending = true;
		}
		return "users";
	}
	
	@RequestMapping(value = "/users/orderByFunctions", method = RequestMethod.GET)
	public String orderByFunctions(Locale locale, Model model) {
		logger.info("orderByFunctions", locale);

		Map<String,Object> attributesMap = model.asMap();
		
		User[] users = (User[]) attributesMap.get("users");
		
		if (sortAscending){
			Arrays.sort(users, new Comparator<User>() {
				@Override
				public int compare(User o1, User o2) {
					return o1.getFunctions().compareTo(o2.getFunctions());
				}
			});
			sortAscending = false;
		}
		else{
			Arrays.sort(users, new Comparator<User>() {
				@Override
				public int compare(User o1, User o2) {
					return o2.getFunctions().compareTo(o1.getFunctions());
				}
			});
			sortAscending = true;
		}
		return "users";
	}

}
