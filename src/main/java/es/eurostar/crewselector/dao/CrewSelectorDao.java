package es.eurostar.crewselector.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import es.eurostar.crewselector.model.Function;
import es.eurostar.crewselector.model.Role;
import es.eurostar.crewselector.model.User;

@Repository
public class CrewSelectorDao {

	private static JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		if (CrewSelectorDao.jdbcTemplate == null)
			CrewSelectorDao.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public boolean existsUserId(int id) {
		if (id == 0)
			return false;
		Integer res = jdbcTemplate.queryForObject(
				"SELECT COUNT(*) from users WHERE iduser=?",
				new Object[] { id }, Integer.class);
		return res > 0;
	}

	public int getUsersCount() {
		return jdbcTemplate.queryForObject("SELECT COUNT(*) from users",
				Integer.class);
	}

	public int getRolesCount() {
		return jdbcTemplate.queryForObject("SELECT COUNT(*) from roles",
				Integer.class);
	}

	public int getFunctionsCount() {
		return jdbcTemplate.queryForObject("SELECT COUNT(*) from functions",
				Integer.class);
	}

	public Role[] getRoles() {
		List<Role> tmp = jdbcTemplate.query(
				"Select idrole,rolename from roles", new RowMapper<Role>() {
					public Role mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						Role c = new Role();
						c.setIdrole(rs.getInt(1));
						c.setRolename(rs.getString(2));
						return c;
					}
				});
		return tmp.toArray(new Role[0]);
	}

	public User[] getUsers() {
		List<User> tmp = jdbcTemplate
				.query("Select iduser,idrole,login,passwd,name,surname,note from users",
						new RowMapper<User>() {
							public User mapRow(ResultSet rs, int rowNum)
									throws SQLException {
								User c = new User();
								c.setIduser(rs.getInt(1));
								c.setIdrole(rs.getInt(2));
								c.setIdLogin(rs.getString(3));
								c.setPasswd(rs.getString(4));
								c.setName(rs.getString(5));
								c.setSurname(rs.getString(6));
								c.setNote(rs.getString(7));
								return c;
							}
						});
		return tmp.toArray(new User[0]);
	}
	
	public Function[] getFuncions (){
		List<Function> tmp = jdbcTemplate.query(
				"Select idrole,functionname from functions order by idrole asc, functionname asc"
				
				, new RowMapper<Function>() {
					public Function mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						Function function = new Function();
						function.setIdrole(rs.getInt(1));
						function.setFunctionname(rs.getString(2));
						return function;
					}
				});
		
		return tmp.toArray(new Function[0]);

	}

	public void saveUser(final User user) {
		final String sqlInsert = "insert into users "
				+ "(idrole,login,passwd,name,surname,note) VALUES (?, ?,?,?,?,?);";
		jdbcTemplate.update(new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con)
					throws SQLException {
				PreparedStatement res= con.prepareStatement(sqlInsert);
				res.setInt(1, user.getIdrole());
				res.setString(2, user.getIdLogin());
				res.setString(3, user.getPasswd());
				res.setString(4, user.getName());
				res.setString(5, user.getSurname());
				res.setString(6, user.getNote());
				return res;
			}
		});
	}
	
	public User isLoginOK(User loggedUser){
		
		List<User> users = jdbcTemplate.query(
								"select * from users where login=? and passwd=?",
								new Object[] { loggedUser.getIdLogin(), loggedUser.getPasswd()},
						new RowMapper<User>() {
							public User mapRow(ResultSet rs, int rowNum)
									throws SQLException {
								User c = new User();
								c.setIduser(rs.getInt(1));
								c.setIdrole(rs.getInt(2));
								c.setIdLogin(rs.getString(3));
								c.setPasswd(rs.getString(4));
								c.setName(rs.getString(5));
								c.setSurname(rs.getString(6));
								c.setNote(rs.getString(7));
								return c;
							}
						});
						
		if (users.size()==0){
			return null;
		}
		
		return users.get(0);
	}

	public Function[] getFuncionsByRole(int idRole) {
		List<Function> tmp = jdbcTemplate.query(
				"Select idrole,functionname from functions where idRole = ? order by functionname asc",
				new Object[] { idRole},
				new RowMapper<Function>() {
					public Function mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						Function function = new Function();
						function.setIdrole(rs.getInt(1));
						function.setFunctionname(rs.getString(2));
						return function;
					}
				});
		
		return tmp.toArray(new Function[0]);
	}
}
