package es.eurostar.crewselector.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.eurostar.crewselector.dao.CrewSelectorDao;
import es.eurostar.crewselector.model.Function;
import es.eurostar.crewselector.model.User;

@Service
public class UserService {
	
	@Autowired
	private CrewSelectorDao userDao;
	
	/**
	 * Get all users that are stored in DB.
	 * @return users
	 */
	public User[] getUsers(){
		
		String[] functionsListByRole = getFunctionsByAllRoles(userDao.getRolesCount());
		
		User[] users = userDao.getUsers();
		
		for (int i = 0;i<users.length;i++){
			users[i].setFunctions(functionsListByRole[users[i].getIdrole()]);
		}
		return users;
	}
	
	/**
	 * Get list of functions stored by id role, comma delimited.
	 * @param numRoles numbers of roles existing in DB
	 * @return functions stored by id role
	 */
	public String[] getFunctionsByAllRoles(int numRoles){
		Function[] functions = userDao.getFuncions();
		
		String[] functionsListByRole = new String[numRoles];
		int currentIdRole=-1;
		StringBuilder sb = new StringBuilder();
		for(Function function: functions){
			if (currentIdRole == function.getIdrole()){
				if (sb.length() > 0){
					sb.append(",");
				}
				sb.append(function.getFunctionname());
			}
			else{
				if (sb.length()>0){
					functionsListByRole[currentIdRole] = sb.toString();
				}
				currentIdRole = function.getIdrole();
				sb = new StringBuilder();
				sb.append(function.getFunctionname());
			}
		}
		functionsListByRole[currentIdRole] = sb.toString();
		
		return functionsListByRole;
	}

	
	/**
	 * Get list of functions for an id role, comma delimited.
	 * @param numRoles numbers of roles existing in DB
	 * @return functions 
	 */
	public String getFunctionsByRoles(int idRole){
		Function[] functions = userDao.getFuncionsByRole(idRole);
		
		StringBuilder sb = new StringBuilder();
		for(Function function: functions){
			if (sb.length() > 0){
				sb.append(",");
			}
			sb.append(function.getFunctionname());
		}
		
		return sb.toString();
	}
}
