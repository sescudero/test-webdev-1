CREATE TABLE users (
  iduser INT IDENTITY
, idrole INT NOT NULL  
, login VARCHAR(40) NOT NULL
, passwd VARCHAR(40) NOT NULL
, name VARCHAR(40) NOT NULL
, surname VARCHAR(80) NOT NULL
, note VARCHAR(255) NOT NULL
);

CREATE TABLE roles (
  idrole INT IDENTITY
, rolename VARCHAR(40) NOT NULL
);

CREATE TABLE functions (
  idfunction INT IDENTITY
, idrole int NOT NULL
, functionname VARCHAR(40) NOT NULL
);

ALTER TABLE users ADD FOREIGN KEY (idrole) REFERENCES roles(idrole);

ALTER TABLE functions ADD FOREIGN KEY (idrole) REFERENCES roles(idrole);

