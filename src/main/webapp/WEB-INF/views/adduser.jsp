<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false"%>
<!DOCTYPE html>
<html>

<head>
<title>Add user</title>
</head>
<body>
	<h1>Selection test for Java Web Developers @ ${serverTime}</h1>
	<h2>Test #2.2 and 2.3</h2>	
	<h4>Test #2.2</h4>
	<p>In this page if you save a new user there is a server side error.
	Find it and get rid of it (hint: the errors could be more than one&hellip;)</p>
	<h4>Test #2.3</h4>
	<p>Do the necessary changes so that this page can be used only by users 
	with the function 'useradmin' linked to their role.</p>
	
<!--  FEEL FREEE TO WRITE EVERYTHING YOU WANT UNDER THIS ROW -->

	<form:form commandName="adduser" modelAttribute="user" method="post">
		<form:label path="idLogin">User</form:label>
		<form:input path="idLogin" type="text" title="insert username"/><br/>
		<form:label path="passwd">Password</form:label>
		<form:input path="passwd" type="password" title="insert password"/><br/>
		<form:label path="name">Name</form:label>
		<form:input path="name" type="text" title="insert name"/><br/>
		<form:label path="surname">Surname</form:label>
		<form:input path="surname" type="text" title="insert surname"/><br/>
		<form:label path="idrole">User role</form:label>
		<form:select path="idrole">
			<form:option value="0">Select role: </form:option>
			<c:forEach items="${roles}" var="role">
				<form:option value="${role.idrole}">${role.rolename}</form:option>
			</c:forEach>
		</form:select><br/>
	
		<form:label path="note">Free notes</form:label>
		<form:textarea path="note" title="free notes"/>
		
		<input name="save" type="submit" value="save"/>
	</form:form>
</body>
</html>
