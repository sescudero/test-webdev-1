<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<!DOCTYPE html>
<html>

<head>
<title>Greetings</title>
</head>
<body>
	<h1>Selection test for Java Web Developers @ ${serverTime}</h1>
	<h4>Test #1.1</h4>
	<p>&hellip;The 'login' button will redirect here 
	if the user and password matches,&hellip;</p>
	
	<!--  FEEL FREEE TO WRITE EVERYTHING YOU WANT UNDER THIS ROW -->
	<p>Welcome, ${loggedUser.name} ${loggedUser.surname}!!!</p>
	
</body>
</html>
