<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<!DOCTYPE html>
<html>

<head>
<title>Home</title>
</head>
<body>
	<h1>Selection test for Java Web Developers @ ${serverTime}</h1>

	<h2>Test description</h2>
	<p>This project has got an in&dash;memory database. with the following schema:</p>
	<h3>Database schema:</h3>
	<p>This test uses a simple database schema, composed of only 3 tables.</p>	
	<ul>
		<li><b>users table</b>: iduser, idrole, login, passw, name, surname,
			note</li>
		<li><b>roles table</b>: idrole, rolename</li>
		<li><b>functions table</b>: idfunction, idrole, functionname</li>
	</ul>
	<p>The users table contains a reference to a record of the roles table, so to be in a N-1 relationship.</p>
	<p>The functions table contains a reference to a record of the roles table, with a N-1 relationship, too.</p>
	<p>So, every user get a role, and every role get one or more functions.</p>
	<p>For example, in the actual data, there is a user with login <em>user2</em>, 
	that is associated to the role <em>operator</em>, 
	that has the functions <em>function1</em>, <em>function2</em>, 
	<em>function5</em>, <em>function6</em></p>
	<p>Another way to see it: users and functions are two tables linked by a N to N relationship, 
	with roles table as the many&dash;to&dash;many relationship holder.</p>
	
	<h3>Actual data:</h3>	
	<p>Every time Tomcat is reloaded, the database is recreated from scratch.
	To speed up things, it is also called a script that loads example data.</p>

	<ul>
		<li>Roles in db: ${rolesCount}.</li>
		<li>Users in db: ${usersCount}.</li>
		<li>Functions in db: ${functionsCount}.</li>
	</ul>
	
	
	<h2>TO BE DONE:</h2>
	<h3>1. Development and UI</h3>
	<h4>Test #1.1</h4>
	<p><a href="login">Here</a> you'll find a login page. Complete it and check user 
	and password. The 'login' button will redirect <a href="greetings">here</a> 
	if the user and password matches, or <a href="wronglogin">here</a> 
	if they don't match.</p>
	<h4>Test #1.2</h4>
	<p><a href="users">Here</a> you'll find a page to list the users. 
	The target is to complete the page to show all the users that are in the database. 
	The users list has to be done in a tabular format, with an header row. 
	If the user does a click on the header of a column, 
	the table will be ordered by that column. 
	The last column of every user must show the list of the functions of the user, 
	comma delimited. 
	So, not a column every function, but all the functions in the same column. </p>
	<h4>Test #1.3 (optional, not for the faint of heart)</h4>
	<p>Create a page that shows the first 5 users using an 
	<a href="http://microformats.org/wiki/hcard">hcard microformat</a>, 
	with a css style freely designed, but that can be read on mobile devices
	without hassles.</p>
	<h3>2. Framework knowledge, problem solving and debug</h3>
	<h4>Test #2.1</h4>
	<p>The page <a href="adduser">adduser</a> is used to create new users. 
	But if you follow the link you see that it brings to this some home page.
	Reconfigure the framework so that the page is reachable.
	 (So, after the modification the previous link will work)</p>
	<h4>Test #2.2</h4>
	<p>In the previous page if you save a new user there is a server side error.
	Find it and get rid of it (hint: the errors could be more than one&hellip;)</p>
	<h4>Test #2.3</h4>
	<p>Do the necessary changes so that the adduser page can be used only by users 
	with the function 'useradmin' linked to their role.</p>
	
	<h2>GOOD LUCK!</h2>
<p>&nbsp;</p><p>&nbsp;</p>
<p>This page is ONLY html: no css, no styles. 
You are free, if you want, to change it and compose a prettier version!</p>
</body>
</html>
