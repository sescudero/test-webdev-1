<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false"%>
<!DOCTYPE html>
<html>

<head>
<title>Login</title>
</head>
<body>
	<h1>Selection test for Java Web Developers @ ${serverTime}</h1>
	<h4>Test #1.1</h4>
	<p>This is a login page. Complete it and check user 
	and password. The 'login' button will redirect <a href="greetings.jsp">here</a> 
	if the user and password matches, or <a href="wronglogin.jsp">here</a> 
	if they don't match.</p>
	
	<!--  FEEL FREEE TO CHANGE THIS PAGE UNTIL IT FITS YOU (AND IT WORKS!) -->	
	
	<form:form method="post" modelAttribute="loginPasswd">
	<label for="user">User</label>
	<form:input id="user" path="idLogin" name="user" type="text" title="insert username"/>
	<label for="password">Password</label>
	<form:input id="password" path="passwd" name="password" type="password" title="insert password"/>
	<input name="login" type="submit" value="login"/>
	</form:form>
	
</body>
</html>
