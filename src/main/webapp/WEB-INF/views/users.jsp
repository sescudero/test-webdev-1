<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<!DOCTYPE html>
<html>

<head>
<title>Users List</title>
</head>
<body>
	<h1>Selection test for Java Web Developers @ ${serverTime}</h1>
	<h4>Test #1.2</h4>
	<p>The target is to complete this page to show all the users that are in the database. 
	The users list has to be done in a tabular format, with an header row. 
	If the user does a click on the header of a column, 
	the table will be ordered by that column. 
	The last column of every user must show the list of the functions of the user, 
	comma delimited. 
	So, not a column every function, but all the functions in the same column. </p>
	<p>Here find the list of all users, but with the wrong format. So that the controller code is half written!</p>
	
	<!--  THE CODE CAN BE ADDED UNDER THIS ROW -->	
	<h1>Users List</h1>
 	<table id="usersTable">
 		<thead>
   			<tr>
      			<th><a href="${pageContext.servletContext.contextPath}/users/orderByIdUser">iduser</a></th>
      			<th><a href="${pageContext.servletContext.contextPath}/users/orderByLogin">login</a></th>      
      			<th><a href="${pageContext.servletContext.contextPath}/users/orderByPasswd">passwd</a></th>
      			<th><a href="${pageContext.servletContext.contextPath}/users/orderByName">name</a></th>
      			<th><a href="${pageContext.servletContext.contextPath}/users/orderBySurname">surname</a></th>
      			<th><a href="${pageContext.servletContext.contextPath}/users/orderByNote">note</a></th>
      			<th><a href="${pageContext.servletContext.contextPath}/users/orderByIdRole">idrole</a></th>
      			<th><a href="${pageContext.servletContext.contextPath}/users/orderByFunctions">functions</a></th>
    		</tr>
  		</thead>
  		<tbody>
 		<c:forEach items="${users}" var="user">
 			<tr>
 				<td>
					${user.iduser}
				</td>
 				<td>
 				    ${user.idLogin}
 				</td>
 				<td>
					${user.passwd}
				</td>
 				<td>
 				    ${user.name}
 				</td>
 				<td>
					${user.surname}
				</td>
 				<td>
 				    ${user.note}
 				</td>
 				<td>
					${user.idrole}
				</td>
 				<td>
 				    ${user.functions}
 				</td>
 			</tr>
		</c:forEach>
  		</tbody>
  		</table>
	
</body>
</html>
